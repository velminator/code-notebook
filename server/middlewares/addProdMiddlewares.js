const path = require('path');
const express = require('express');
const compression = require('compression');

module.exports = function addProdMiddlewares(app, options) {
  const publicPath = options.publicPath || '/';
  const outputPath = options.outputPath || path.resolve(process.cwd(), 'build');

  // compression middleware compresses your server responses which makes them
  // smaller (applies also to assets). You can read more about that technique
  // and other good practices on official Express.js docs http://mxs.is/googmy
  app.use(compression());
  app.use(publicPath, express.static(outputPath));

  app.use(
    '/stackedit',
    express.static(path.join(path.resolve('.'), 'stackedit/')),
  );
  app.use(
    '/static',
    express.static(path.join(path.resolve('.'), 'stackedit/static/')),
  );

  app.get('stackedit/*', (req, res) => {
    res.sendFile(path.join(path.resolve('.'), 'stackedit/index.html'));
  });

  app.get('*', (req, res) =>
    res.sendFile(path.resolve(outputPath, 'index.html')),
  );
};
