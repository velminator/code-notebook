import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Replace this with your own config details
const config = {
  apiKey: 'AIzaSyDrHnOqggMlTxSY6g1iqjyPYgirLkCt1qE',
  authDomain: 'code-notebook.firebaseapp.com',
  databaseURL: 'https://code-notebook.firebaseio.com',
  projectId: 'code-notebook',
  storageBucket: 'code-notebook.appspot.com',
  messagingSenderId: '409876554052',
};
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export const db = firebase.firestore();
export const dbTimeStamp = firebase.firestore.FieldValue.serverTimestamp;

export const mergeResults = (srcList, targetList, key) => {
  const data = {};
  srcList.forEach(record => {
    data[record.id] = record;
  });
  targetList.forEach(record => {
    data[record[key]] = record;
  });
  return Object.values(data);
};
