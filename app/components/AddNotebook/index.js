/**
 *
 * AddNotebook
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tooltip from '@material-ui/core/Tooltip';
import green from '@material-ui/core/colors/green';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import withUserAuth from 'containers/UserAuth';
import { db, dbTimeStamp } from 'utils/firebase';
// import styled from 'styled-components';

const styles = {
  add: {
    right: '52px',
    position: 'fixed',
    bottom: '20px',
  },
  paper: {
    width: '500px',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
};

/* eslint-disable react/prefer-stateless-function */
class AddNotebook extends React.PureComponent {
  state = {
    open: false,
    name: '',
    loading: false,
    success: false,
    isPublic: false,
  };

  handleTextChange = event => {
    this.setState({ name: event.target.value });
  };

  handleSwitchChange = event => {
    this.setState({ isPublic: event.target.checked });
  };

  handleClickOpen = () => {
    if (this.props.authIsLoggedIn) {
      this.setState({ open: true });
    } else {
      this.props.authSignInAction();
    }
  };

  handleClose = () => {
    this.setState({ open: false, name: '' });
  };

  handleCreate = () => {
    this.setState({ success: false, loading: true });
    const { uid, displayName } = this.props.authUser;
    const book = {
      cover_id: 1 + Math.floor(Math.random() * Math.floor(20)),
      created_time: dbTimeStamp(),
      description: '',
      last_accessed_time: dbTimeStamp(),
      last_modified_time: dbTimeStamp(),
      name: this.state.name,
      isPublic: this.state.isPublic,
      createdByUid: uid,
      createdByDisplayName: displayName,
    };

    const dbCreate = async function dbCreate() {
      const result = await db.collection('books').add(book);
      return result;
    };
    dbCreate();
    this.setState({ success: false, loading: false, open: false, name: '' });
    this.props.refresh();
  };

  render() {
    const { classes } = this.props;
    const { loading, success, open, name } = this.state;
    const buttonClassname = classNames({
      [classes.buttonSuccess]: success,
    });
    return (
      <div>
        <div className={classes.add}>
          <Tooltip title="Add">
            <Button
              variant="fab"
              color="secondary"
              aria-label="Add"
              onClick={this.handleClickOpen}
            >
              <AddIcon />
            </Button>
          </Tooltip>
        </div>
        <div>
          <div>
            <Dialog
              open={open}
              onClose={this.handleClose}
              aria-labelledby="form-dialog-title"
              classes={{ paper: classes.paper }}
            >
              <DialogTitle id="form-dialog-title">
                Create New Notebook
              </DialogTitle>
              <DialogContent>
                <TextField
                  required
                  autoFocus
                  margin="dense"
                  id="notebook"
                  label="Notebook Name"
                  type="search"
                  fullWidth
                  value={name}
                  onChange={this.handleTextChange}
                  disabled={loading}
                />
                <FormControlLabel
                  control={
                    <Switch
                      checked={this.state.isPublic}
                      onChange={this.handleSwitchChange}
                      value=""
                    />
                  }
                  label="Available to public"
                />
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={this.handleClose}
                  color="primary"
                  disabled={loading}
                >
                  Cancel
                </Button>
                <Button
                  onClick={this.handleCreate}
                  color="primary"
                  className={buttonClassname}
                  disabled={loading || this.state.name.length === 0}
                >
                  Create
                </Button>
                {loading && (
                  <CircularProgress
                    size={24}
                    className={classes.buttonProgress}
                  />
                )}
              </DialogActions>
            </Dialog>
          </div>
        </div>
      </div>
    );
  }
}

AddNotebook.propTypes = {
  classes: PropTypes.object,
  refresh: PropTypes.func.isRequired,
  authIsLoggedIn: PropTypes.bool.isRequired,
  authSignInAction: PropTypes.func.isRequired,
  authUser: PropTypes.object.isRequired,
};

export default withUserAuth(withStyles(styles)(AddNotebook));
