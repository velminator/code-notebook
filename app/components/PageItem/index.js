/**
 *
 * PageItem
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ReactMarkdown from 'react-markdown';
import Button from '@material-ui/core/Button';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withUserAuth from 'containers/UserAuth';
import EditPage from 'components/EditPage';
import { db } from 'utils/firebase';
import { CodeBlock } from './CodeBlock';

const styles = theme => ({
  paper: {
    padding: '20px',
    margin: '10px',
    position: 'relative',
    minHeight: '200px',
  },
  markupPreviewCollapse: {
    backgroundColor: '#F3F3F3',
    padding: '10px',
    maxHeight: '400px',
    overflow: 'hidden',
    marginTop: '20px',
  },
  markupPreviewExpand: {
    backgroundColor: '#F3F3F3',
    padding: '10px',
  },
  button: {
    position: 'absolute',
    right: '20px',
    bottom: '10px',
  },
  controlButtonDiv: {
    position: 'absolute',
    right: '2%',
    top: '-5px',
    display: 'block',
  },
  controlButton: {
    margin: theme.spacing.unit / 4,
  },
});

/* eslint-disable react/prefer-stateless-function */
class PageItem extends React.Component {
  state = {
    expanded: false,
    dialogOpen: false,
    openEditPage: false,
  };

  componentDidMount() {
    // this.renderMarkup();
    // <div name="marupPreview" className={classes.markup} dangerouslySetInnerHTML={{__html:this.state.markup}} />-->
  }

  deleteItem(docId) {
    const dbDelete = async function dbDelete() {
      const result = await db.collection('notes').doc(docId);
      result.delete();
    };
    dbDelete();
    this.handleClose();
    this.props.refresh(this.props.bookId, this.props.authUser.uid);
  }

  toggleExpand = () => {
    this.setState(state => ({
      expanded: !state.expanded,
    }));
  };

  deleteHandler = () => {
    if (this.props.deleteCb) {
      this.props.deleteCb();
    } else {
      this.deleteItem(this.props.pageId);
    }
  };

  editHandler = () => {
    if (this.props.authIsLoggedIn) {
      if (this.props.editCb) {
        this.props.editCb();
      } else {
        this.setState({
          openEditPage: true,
        });
      }
    } else {
      this.props.authSignInAction();
    }
  };

  handleClose = () => {
    this.setState({ dialogOpen: false });
  };

  handleOpen = () => {
    if (this.props.authIsLoggedIn) {
      this.setState({ dialogOpen: true });
    } else {
      this.props.authSignInAction();
    }
  };

  onEditCloseCb = () => {
    this.setState({
      openEditPage: false,
    });
  };

  renderControls() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Tooltip title="Delete" placement="top">
          <IconButton
            onClick={this.handleOpen}
            className={classes.controlButton}
            aria-label="Delete"
          >
            <DeleteIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Edit" placement="top">
          <IconButton
            onClick={this.editHandler}
            className={classes.controlButton}
            aria-label="Edit"
          >
            <EditIcon />
          </IconButton>
        </Tooltip>
      </React.Fragment>
    );
  }

  renderExpandCollapseButtonText() {
    if (this.state.expanded) {
      return (
        <React.Fragment>
          <ExpandLess />
          Collapse
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <ExpandMore />
        Expand
      </React.Fragment>
    );
  }

  renderDeleteDialog() {
    const { data } = this.props;
    return (
      <Dialog
        open={this.state.dialogOpen}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Confirm Delete</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure, you want to delete{' '}
            {this.props.deleteCb ? 'markup' : data.title} ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            No
          </Button>
          <Button onClick={this.deleteHandler} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  render() {
    const { classes, data } = this.props;
    return (
      <Paper className={classes.paper}>
        <div className={classes.controlButtonDiv}>{this.renderControls()}</div>
        <Typography variant="h6" gutterBottom>
          {data.title}
        </Typography>
        <div
          name="markup"
          className={
            this.state.expanded
              ? classes.markupPreviewExpand
              : classes.markupPreviewCollapse
          }
        >
          <ReactMarkdown source={data.markup} renderers={{ code: CodeBlock }} />
        </div>
        <Button onClick={this.toggleExpand} className={classes.button}>
          {this.renderExpandCollapseButtonText()}
        </Button>
        {this.renderDeleteDialog()}
        {this.state.openEditPage && (
          <EditPage
            pageId={this.props.pageId}
            refresh={this.props.refresh}
            authUser={this.props.authUser}
            bookId={this.props.bookId}
            onClose={this.onEditCloseCb}
            pageData={data}
          />
        )}
        <a href={data.replLink} target="_blank">
          Repl Link
        </a>
      </Paper>
    );
  }
}

PageItem.propTypes = {
  classes: PropTypes.object,
  data: PropTypes.object.isRequired,
  refresh: PropTypes.func,
  pageId: PropTypes.string.isRequired,
  bookId: PropTypes.string.isRequired,
  editCb: PropTypes.func,
  deleteCb: PropTypes.func,
  authIsLoggedIn: PropTypes.bool.isRequired,
  authSignInAction: PropTypes.func.isRequired,
  authUser: PropTypes.object.isRequired,
};

export default withUserAuth(withStyles(styles)(PageItem));
