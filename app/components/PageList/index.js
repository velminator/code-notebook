/**
 *
 * PageList
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';
import PageItem from 'components/PageItem';

const styles = {};

/* eslint-disable react/prefer-stateless-function */
class PageList extends React.Component {
  render() {
    const { pageList } = this.props;
    return (
      <React.Fragment>
        {pageList.map(page => (
          <Grid key={page.id} item xs={12} lg={12} md={12}>
            <PageItem
              data={page.data()}
              refresh={this.props.refresh}
              pageId={page.id}
              bookId={this.props.bookId}
            />
          </Grid>
        ))}
      </React.Fragment>
    );
  }
}

PageList.propTypes = {
  pageList: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  refresh: PropTypes.func,
  bookId: PropTypes.string.isRequired,
};

export default withStyles(styles)(PageList);
