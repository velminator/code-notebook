/**
 *
 * AddPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import withUserAuth from 'containers/UserAuth';
import EditPage from 'components/EditPage';

/* eslint-disable react/prefer-stateless-function */
const styles = () => ({
  add: {
    right: '52px',
    position: 'fixed',
    bottom: '20px',
  },
});
class AddPage extends React.Component {
  state = {
    openEditPage: false,
  };

  handleOpenEditPage = () => {
    if (this.props.authIsLoggedIn) {
      this.setState({
        openEditPage: true,
      });
    } else {
      this.props.authSignInAction();
    }
  };

  onEditCloseCb = () => {
    this.setState({
      openEditPage: false,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <div className={classes.add}>
          <Tooltip title="Add">
            <Button
              variant="fab"
              color="secondary"
              aria-label="Add"
              onClick={this.handleOpenEditPage}
            >
              <AddIcon />
            </Button>
          </Tooltip>
          {this.state.openEditPage && (
            <EditPage
              refresh={this.props.refresh}
              authUser={this.props.authUser}
              bookId={this.props.bookId}
              onClose={this.onEditCloseCb}
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

AddPage.propTypes = {
  bookId: PropTypes.string.isRequired,
  classes: PropTypes.object,
  refresh: PropTypes.func,
  authIsLoggedIn: PropTypes.bool.isRequired,
  authSignInAction: PropTypes.func.isRequired,
  authUser: PropTypes.object.isRequired,
};

export default withUserAuth(withStyles(styles)(AddPage));
