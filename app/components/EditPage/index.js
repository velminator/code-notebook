/**
 *
 * EditPage
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import green from '@material-ui/core/colors/green';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Chip from '@material-ui/core/Chip';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import { db, dbTimeStamp } from 'utils/firebase';
import LinearProgress from '@material-ui/core/LinearProgress';
import Stackedit from 'stackedit-js';
import Paper from '@material-ui/core/Paper';
import PageItem from 'components/PageItem';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ContentPlaceHolderImage from './contentPlaceholder.png';
import ReplPlaceHolderImage from './replPlaceholder.svg';

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  paper: {
    width: '500px',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  titleField: {
    margin: theme.spacing.unit,
    paddingTop: '40px',
  },
  tagField: {
    margin: theme.spacing.unit,
    width: 200,
  },
  chip: {
    margin: theme.spacing.unit / 2,
  },
  markupContent: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    height: '440px',
    overflow: 'auto',
  },
  markupDivPlaceHolder: {
    backgroundImage: `url(${ContentPlaceHolderImage})`,
    height: '220px',
    textAlign: 'right',
    verticalAlign: 'middle',
    padding: '20px',
    margin: '20px',
    display: 'block',
    cursor: 'pointer',
    ':hover': {
      border: '1px solid black',
    },
  },
  chipRoot: {
    backgroundColor: '#0097e6',
  },
  chipLabel: {
    color: '#f5f6fa',
  },
  chipDeleteIcon: {
    color: '#dcdde1',
  },
  replDivPlaceHolder: {
    backgroundImage: `url(${ReplPlaceHolderImage})`,
    height: '220px',
    textAlign: 'right',
    verticalAlign: 'right',
    padding: '20px',
    margin: '20px',
    display: 'block',
    cursor: 'pointer',
    ':hover': {
      border: '1px solid black',
    },
  },
  replDialogPaper: {
    width: 800,
  },
  replTextField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  replProjectSelectMenu: {
    width: 200,
  },
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const replProjects = [
  'python3',
  'ruby',
  'nodejs',
  'html',
  'scheme',
  'csharp',
  'java',
  'clojure',
  'haskell',
  'kotlin',
  'lua',
  'python',
  'php',
  'go',
  'cpp',
  'cpp11',
  'c',
  'fsharp',
  'rust',
  'swift',
  'python_turtle',
  'rlang',
  'bash',
  'javascript',
  'coffeescript',
  'roy',
  'django',
  'express',
  'sinatra',
  'rails',
  'nextjs',
  'gatsbyjs',
  'reactjs',
  'reactts',
  'reactre',
  'enzyme',
  'jest',
  'qbasic',
  'forth',
  'apl',
  'lolcode',
  'brainfuck',
  'emoticon',
  'bloop',
  'unlambda',
];

/* eslint-disable react/prefer-stateless-function */
class EditPage extends React.PureComponent {
  state = {
    open: true,
    loading: false,
    // success: false,
    title: '',
    tagText: '',
    chips: [],
    showContentPlaceholder: true,
    showReplPlaceholder: true,
    markup: '',
    replLink: '',
    replProjectType: '',
    replAdded: false,
    isPublic: false,
    rednerFromProps: true,
  };

  handleDialogClose = () => {
    this.setState({
      open: false,
      title: '',
      tagText: '',
      chips: [],
      showContentPlaceholder: true,
      showReplPlaceholder: true,
      markup: '',
      replLink: '',
      replProjectType: '',
      replAdded: false,
    });
    this.props.onClose();
  };

  handleDialogSave = () => {
    this.setState({
      loading: true,
    });
    const { uid, displayName } = this.props.authUser;
    if (this.props.pageId) {
      const { pageId } = this.props;
      const page = {
        markup: this.state.markup,
        title: this.state.title,
        replProjectType: this.state.replProjectType,
        replLink: this.state.replLink,
        last_accessed_time: dbTimeStamp(),
        last_modified_time: dbTimeStamp(),
        tags: this.state.chips,
        isPublic: this.state.isPublic,
      };
      const dbUpdate = async function dbUpdate() {
        const result = await db
          .collection('notes')
          .doc(pageId)
          .update(page);
        return result;
      };
      dbUpdate();
    } else {
      const page = {
        created_time: dbTimeStamp(),
        book: db.doc(`books/${this.props.bookId}`),
        markup: this.state.markup,
        title: this.state.title,
        replProjectType: this.state.replProjectType,
        replLink: this.state.replLink,
        last_accessed_time: dbTimeStamp(),
        last_modified_time: dbTimeStamp(),
        tags: this.state.chips,
        isPublic: this.state.isPublic,
        createdByUid: uid,
        createdByDisplayName: displayName,
      };
      const dbCreate = async function dbCreate() {
        const result = await db.collection('notes').add(page);
        return result;
      };
      dbCreate();
    }
    this.setState({
      open: false,
      loading: false,
      title: '',
      tagText: '',
      chips: [],
      showContentPlaceholder: true,
      showReplPlaceholder: true,
      markup: '',
      replLink: '',
      replProjectType: '',
      replAdded: false,
      isPublic: false,
    });
    this.props.refresh(this.props.bookId);
    this.props.onClose();
  };

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      this.setState(state => ({
        chips: [...state.chips, state.tagText],
        tagText: '',
      }));
    }
  };

  handleChipDelete = data => () => {
    if (!this.state.loading) {
      this.setState(state => {
        const chips = [...state.chips];
        const chipToDelete = chips.indexOf(data);
        chips.splice(chipToDelete, 1);
        return { chips };
      });
    }
  };

  openStackEditor = () => {
    const stackEdit = new Stackedit({
      url: '/stackedit/#',
    });
    stackEdit.openFile({
      name: 'new file', // with an optional filename
      content: {
        text: this.state.markup || 'Enter your Markup Here!', // and the Markdown content.
      },
    });
    const self = this;
    stackEdit.on('fileChange', file => {
      self.setState({
        markup: file.content.text,
        showContentPlaceholder: false,
      });
    });
  };

  deleteMarkup = () => {
    this.setState({
      markup: '',
      showContentPlaceholder: true,
    });
  };

  editMarkup = () => {
    this.openStackEditor();
  };

  toggleReplDialog = () => {
    this.setState(state => ({
      showReplPlaceholder: !state.showReplPlaceholder,
    }));
  };

  handleReplDialog = name => () => {
    if (name) {
      if (name === 'cancel') {
        this.setState(state => ({
          showReplPlaceholder: !state.showReplPlaceholder,
          replLink: '',
          replProjectType: '',
        }));
      } else {
        this.setState(state => ({
          showReplPlaceholder: !state.showReplPlaceholder,
          replAdded: true,
        }));
      }
    }
  };

  handleSwitchChange = event => {
    this.setState({ isPublic: event.target.checked });
  };

  renderReplDialog() {
    const { classes } = this.props;
    const { loading } = this.state;
    return (
      <Dialog
        open
        onClose={this.handleClose}
        aria-labelledby="form-dialog-title"
        classes={{ paper: classes.replDialogPaper }}
      >
        <DialogTitle id="form-dialog-title">Add new Repl Link</DialogTitle>
        <DialogContent>
          <TextField
            id="select-repl-type"
            select
            label="Select"
            disabled={loading}
            className={classes.replTextField}
            value={this.state.replProjectType}
            onChange={this.handleChange('replProjectType')}
            SelectProps={{
              MenuProps: {
                className: classes.replProjectSelectMenu,
              },
            }}
            helperText="Please select your repl project type"
            margin="normal"
          >
            {replProjects.map(option => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            required
            autoFocus
            margin="dense"
            id="repl-url"
            label="Repl Url"
            type="search"
            disabled={loading}
            fullWidth
            value={this.state.replLink}
            onChange={this.handleChange('replLink')}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleReplDialog('cancel')} color="primary">
            Cancel
          </Button>
          <Button
            onClick={this.handleReplDialog('add')}
            color="primary"
            disabled={
              this.state.replLink.length === 0 ||
              this.state.replProjectType.length === 0
            }
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  renderMarkupConten() {
    const { classes } = this.props;
    if (this.state.showContentPlaceholder) {
      return (
        <Paper>
          <div
            role="button"
            tabIndex={0}
            onClick={this.openStackEditor}
            onKeyPress={this.openStackEditor}
            className={classes.markupDivPlaceHolder}
          >
            <Typography variant="h4" gutterBottom>
              Click to add markdown text
            </Typography>
          </div>
        </Paper>
      );
    }
    return (
      <Paper>
        <PageItem
          data={{ markup: this.state.markup }}
          editCb={this.editMarkup}
          deleteCb={this.deleteMarkup}
        />
      </Paper>
    );
  }

  renderReplConten() {
    const { classes } = this.props;
    if (this.state.showReplPlaceholder && !this.state.replAdded) {
      return (
        <Paper>
          <div
            role="button"
            tabIndex={0}
            onClick={this.toggleReplDialog}
            onKeyPress={this.toggleReplDialog}
            className={classes.replDivPlaceHolder}
          >
            <Typography variant="h4" gutterBottom>
              Click to add code repl
            </Typography>
          </div>
        </Paper>
      );
    }
    return (
      <Paper>
        <TextField
          id="select-repl-type"
          select
          label="Select"
          className={classes.replTextField}
          value={this.state.replProjectType}
          onChange={this.handleChange('replProjectType')}
          SelectProps={{
            MenuProps: {
              className: classes.replProjectSelectMenu,
            },
          }}
          helperText="Please select your repl project type"
          margin="normal"
        >
          {replProjects.map(option => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          required
          autoFocus
          margin="dense"
          id="repl-url"
          label="Repl Url"
          type="search"
          fullWidth
          value={this.state.replLink}
          onChange={this.handleChange('replLink')}
        />
      </Paper>
    );
  }

  renderChips() {
    const { classes } = this.props;
    return this.state.chips.map(chip => (
      <Chip
        key={chip}
        classes={{
          root: classes.chipRoot,
          label: classes.chipLabel,
          deleteIcon: classes.chipDeleteIcon,
        }}
        label={chip}
        onDelete={this.handleChipDelete(chip)}
        className={classes.chip}
      />
    ));
  }

  render() {
    const { classes, pageData } = this.props;
    if (pageData && this.state.rednerFromProps) {
      const {
        title,
        isPublic,
        tags,
        replLink,
        replProjectType,
        markup,
      } = pageData;
      this.setState({
        title,
        chips: tags,
        markup,
        replLink,
        replProjectType,
        isPublic,
        rednerFromProps: false,
        showContentPlaceholder: false,
        replAdded: true,
      });
    }

    const { loading } = this.state;
    return (
      <React.Fragment>
        <Dialog
          fullScreen
          open={this.state.open}
          onClose={this.handleDialogClose}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar} color="default">
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.handleDialogClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
              <Typography
                variant="body2"
                gutterBottom
                color="inherit"
                className={classes.flex}
              >
                Add New Page
              </Typography>
              <Button color="inherit" onClick={this.handleDialogSave}>
                save
              </Button>
            </Toolbar>
          </AppBar>
          <DialogContent>
            {this.state.loading && (
              <React.Fragment>
                <LinearProgress /> <br /> <LinearProgress color="secondary" />
              </React.Fragment>
            )}
            <TextField
              required
              autoFocus
              margin="dense"
              id="pageTitle"
              label="Enter Title"
              type="search"
              fullWidth
              value={this.state.title}
              onChange={this.handleChange('title')}
              disabled={loading}
            />
            <TextField
              className={classes.tagField}
              margin="dense"
              id="tags"
              label="Enter Tags"
              type="search"
              fullWidth
              value={this.state.tagText}
              onChange={this.handleChange('tagText')}
              disabled={loading}
              inputProps={{ onKeyPress: this.handleKeyPress }}
            />
            {this.renderChips()}
            {this.renderMarkupConten()}
            {this.renderReplConten()}
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.isPublic}
                  onChange={this.handleSwitchChange}
                  value=""
                />
              }
              label="Available to public"
            />
            {!this.state.showReplPlaceholder && this.renderReplDialog()}
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}

EditPage.propTypes = {
  bookId: PropTypes.string.isRequired,
  classes: PropTypes.object,
  refresh: PropTypes.func,
  authUser: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  pageData: PropTypes.object,
  pageId: PropTypes.string.isRequired,
};

export default withStyles(styles)(EditPage);
