/**
 *
 * Notebook
 *
 */

import React from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Zoom from '@material-ui/core/Zoom';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

const styles = {
  book: {
    width: '144px',
    height: '242px',
    cursor: 'pointer',
    display: 'inline-block',
  },
  bookWrap: {
    textAlign: 'center',
  },
  rounded: {
    borderBottomRightRadius: '8px',
    borderTopRightRadius: '8px',
    borderBottomLeftRadius: '0px',
    borderTopLeftRadius: '0px',
  },
};
function importAll(r) {
  const images = {};
  r.keys().forEach(item => {
    images[item.replace('./', '')] = r(item);
  });
  return images;
}

const images = importAll(
  require.context('images/notebookBackgrounds', false, /\.(png|jpe?g|svg)$/),
);

class Notebook extends React.Component {
  openBook = () => {
    const { history } = this.props;
    history.push(`/notes/${this.props.name}/${this.props.id}/`);
  };

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const { classes, name, image } = this.props;
    return (
      <React.Fragment>
        <Zoom key={this.props.id} in>
          <div className={classes.bookWrap}>
            <Tooltip title={`Click to open ${name} notebook`} placement="top">
              <Paper
                onClick={this.openBook}
                elevation={4}
                classes={{ rounded: classes.rounded }}
                className={classes.book}
                style={{ backgroundImage: `url(${images[image]})` }}
              />
            </Tooltip>
            <Typography variant="body2" gutterBottom align="center">
              {this.props.name}
            </Typography>
          </div>
        </Zoom>
      </React.Fragment>
    );
  }
}

Notebook.propTypes = {
  classes: PropTypes.object,
  id: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
};

export default withStyles(styles)(withRouter(Notebook));
