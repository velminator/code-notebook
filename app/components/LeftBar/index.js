/**
 *
 * LeftBar
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import Note from '@material-ui/icons/Note';
import Person from '@material-ui/icons/Person';
import Search from '@material-ui/icons/Search';
import Drawer from '@material-ui/core/Drawer';
import PropTypes from 'prop-types';
import PowerOffTwoTone from '@material-ui/icons/PowerOffTwoTone';
import Snackbar from '@material-ui/core/Snackbar';
import withUserAuth from 'containers/UserAuth';
import UserProfile from 'components/UserProfile';

// import styled from 'styled-components';
const drawerWidth = '80px';
const styles = theme => ({
  root: {
    height: '100%',
  },
  bottomOption: {
    position: 'absolute',
    bottom: 0,
  },
  drawer: {
    left: drawerWidth,
    width: '440px',
    height: '100%',
    zIndex: 9,
    backgroundColor: '#F7F7F7',
  },
  modal: {
    position: 'absolute',
    left: drawerWidth,
  },
  icon: {
    marginBottom: theme.spacing.unit,
  },
  option: {
    boxShadow: 'none',
    backgroundColor: 'none',
    margin: '16px auto',
    width: '100%',
    display: 'inline-block',
    textAlign: 'center',
    cursor: 'pointer',
    border: 'none',
    textTransform: 'None',
  },
  userSettings: {
    width: '100%',
    height: '100%',
  },
  userAvatar: {
    margin: 10,
    width: 120,
    height: 120,
  },
  closeSnackbarIcon: {
    padding: theme.spacing.unit / 2,
  },
});

/* eslint-disable react/prefer-stateless-function */
class LeftBar extends React.PureComponent {
  state = {
    userMenu: false,
    showSnackbar: true,
  };

  handleSnackBarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ showSnackbar: false });
  };

  logout = () => {
    this.props.authSignOutAction();
    this.setState(prevState => ({
      userMenu: !prevState.userMenu,
      showSnackbar: false,
    }));
  };

  toggleDrawer = () => {
    if (this.props.authIsLoggedIn) {
      this.setState(prevState => ({
        userMenu: !prevState.userMenu,
        showSnackbar: false,
      }));
    } else {
      this.props.authSignInAction();
    }
  };

  render() {
    const { classes, authUser } = this.props;
    return (
      <div>
        <Drawer
          open={this.state.userMenu}
          onClose={this.toggleDrawer}
          classes={{ paper: classes.drawer, modal: classes.modal }}
        >
          <div className={classes.userSettings}>
            <UserProfile authUser={authUser} />
            <Button
              variant="text"
              className={classes.option}
              onClick={this.logout}
            >
              <PowerOffTwoTone className={classes.icon} />
              Logout
            </Button>
          </div>
        </Drawer>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.showSnackbar}
          autoHideDuration={6000}
          onClose={this.handleSnackBarClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={
            <span id="message-id">Can create/update only when logged in!</span>
          }
          action={[
            <Button
              key="undo"
              color="secondary"
              size="small"
              onClick={this.toggleDrawer}
            >
              Login
            </Button>,
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.closeSnackbarIcon}
              onClick={this.handleSnackBarClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
        <div className={classes.root}>
          <Button
            component={Link}
            to="/"
            variant="text"
            className={classes.option}
          >
            <LibraryBooks className={classes.icon} />
            Books
          </Button>
          <Button
            component={Link}
            to="/notes/All Notes/all"
            variant="text"
            className={classes.option}
          >
            <Note className={classes.icon} />
            Notes
          </Button>
          <Button
            component={Link}
            to="/search"
            variant="text"
            className={classes.option}
          >
            <Search className={classes.icon} />
            Search
          </Button>
          <div className={classes.bottomOption}>
            <Button
              variant="text"
              className={classes.option}
              onClick={this.toggleDrawer}
            >
              <Person className={classes.icon} />
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

LeftBar.propTypes = {
  classes: PropTypes.object.isRequired,
  authIsLoggedIn: PropTypes.bool.isRequired,
  authSignInAction: PropTypes.func.isRequired,
  authUser: PropTypes.object.isRequired,
  authSignOutAction: PropTypes.func.isRequired,
};

export default withUserAuth(withStyles(styles)(LeftBar));
