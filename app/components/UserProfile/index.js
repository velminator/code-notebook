/**
 *
 * UserProfile
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

const styles = {
  text: {
    margin: 10,
    padding: 10,
  },
  userAvatar: {
    width: 100,
    height: 100,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: 20,
  },
};

function UserProfile(props) {
  const { classes, authUser } = props;
  return (
    <React.Fragment>
      <Avatar
        alt="Remy Sharp"
        src={authUser.photoURL}
        className={classes.userAvatar}
      />
      <Typography variant="h6" align="center" gutterBottom classes>
        {authUser.displayName}
      </Typography>
      <Divider />
      <div className={classes.text}>
        <Typography variant="subtitle2" gutterBottom>
          {authUser.email}
        </Typography>
        <Typography variant="subtitle2" gutterBottom>
          User id: {authUser.uid}
        </Typography>
      </div>
      <Divider />
    </React.Fragment>
  );
}

UserProfile.propTypes = {
  classes: PropTypes.object,
  authUser: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserProfile);
