/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  GET_LATEST_NOTEBOOK_LIST,
  GET_LATEST_NOTEBOOK_LIST_SUCCESS,
  GET_LATEST_NOTEBOOK_LIST_ERROR,
  ADD_NOTEBOOK,
} from './constants';

export function getLatestNotebookList(userUid) {
  return {
    type: GET_LATEST_NOTEBOOK_LIST,
    userUid,
  };
}

/**
 * Dispatched when the Notebooks are loaded by the request saga
 *
 * @param  {array} notebooks array
 *
 * @return {object}      An action object with a type of GET_LATEST_NOTEBOOKS_LIST_SUCCESS passing the repos
 */
export function notebookListLoaded(snapshot) {
  const data = [];
  snapshot.forEach(record => {
    data.push(record);
  });
  return {
    type: GET_LATEST_NOTEBOOK_LIST_SUCCESS,
    data,
  };
}

/**
 * Dispatched when loading the perfTests fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of GET_LATEST_LIST_ERROR passing the error
 */
export function notebookListLoadingError(error) {
  return {
    type: GET_LATEST_NOTEBOOK_LIST_ERROR,
    error,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of ADD_NOTEBOOK
 */
export function addNotebook(name) {
  return {
    type: ADD_NOTEBOOK,
    name,
  };
}
