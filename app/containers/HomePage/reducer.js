/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';
import {
  GET_LATEST_NOTEBOOK_LIST,
  GET_LATEST_NOTEBOOK_LIST_SUCCESS,
  GET_LATEST_NOTEBOOK_LIST_ERROR,
} from './constants';

// The initial state of the App
export const initialState = fromJS({
  loading: true,
  error: false,
  notebookList: [],
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LATEST_NOTEBOOK_LIST:
      return state
        .set('loading', true)
        .set('error', false)
        .set('notebookList', []);
    case GET_LATEST_NOTEBOOK_LIST_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('notebookList', action.data);
    case GET_LATEST_NOTEBOOK_LIST_ERROR:
      return state.set('error', action.error).set('loading', false);
    default:
      return state;
  }
}

export default homeReducer;
