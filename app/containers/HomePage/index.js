/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Notebook from 'components/Notebook';
import AddNotebook from 'components/AddNotebook';
import withUserAuth from 'containers/UserAuth';
import { selectGetNotebookList, selectLoading, selectError } from './selectors';
import { getLatestNotebookList } from './actions';
import reducer from './reducer';
import saga from './saga';

const styles = {
  bookContainer: {
    flexGrow: 1,
    padding: '10px',
  },
  title: {
    width: '80vw',
    // height: "200px",
    padding: '10px',
  },
};
/* eslint-disable react/prefer-stateless-function */
class HomePage extends React.PureComponent {
  componentDidMount() {
    const { uid } = this.props.authUser;
    this.props.getLatestList(uid);
  }

  renderResults() {
    if (this.props.loading) {
      return (
        <Grid item xs={12}>
          <div style={{ textAlign: 'center' }}>
            <CircularProgress size={80} />
          </div>
        </Grid>
      );
    }
    if (this.props.error) {
      return <h1> Error! </h1>;
    }
    return (
      <React.Fragment>
        {this.props.notebookList.map(data => (
          <Grid key={data.id} item xs={12} lg={2} md={6}>
            <Notebook
              key={data.id}
              id={data.id}
              name={data.data().name}
              image={`${data.data().cover_id}.jpg`}
            />
          </Grid>
        ))}
      </React.Fragment>
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.bookContainer}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Typography
              classes={{ root: classes.title }}
              variant="h6"
              gutterBottom
              align="center"
            >
              Notebooks
            </Typography>
          </Grid>
          {this.renderResults()}
        </Grid>
        <AddNotebook refresh={this.props.getLatestList} />
      </div>
    );
  }
}
HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  getLatestList: PropTypes.func,
  notebookList: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  classes: PropTypes.object,
  authUser: PropTypes.object.isRequired,
};

export function mapDispatchToProps(dispatch) {
  return {
    getLatestList: userUid => dispatch(getLatestNotebookList(userUid)),
  };
}

const mapStateToProps = createStructuredSelector({
  loading: selectLoading(),
  error: selectError(),
  notebookList: selectGetNotebookList(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default withUserAuth(
  compose(
    withReducer,
    withSaga,
    withConnect,
  )(withStyles(styles)(HomePage)),
);
