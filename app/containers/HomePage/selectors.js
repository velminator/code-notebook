/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Homepage selectors
 */

const selectHome = state => state.get('home', initialState);

const selectGetNotebookList = () =>
  createSelector(selectHome, homeState => homeState.get('notebookList'));

const selectLoading = () =>
  createSelector(selectHome, homeState => homeState.get('loading'));

const selectError = () =>
  createSelector(selectHome, homeState => homeState.get('error'));

export { selectHome, selectGetNotebookList, selectLoading, selectError };
