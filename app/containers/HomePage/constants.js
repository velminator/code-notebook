/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const GET_LATEST_NOTEBOOK_LIST =
  'code-notebook/HomePage/GET_LATEST_NOTEBOOK_LIST';

export const GET_LATEST_NOTEBOOK_LIST_SUCCESS =
  'code-notebook/HomePage/GET_LATEST_NOTEBOOK_LIST_SUCCESS';

export const GET_LATEST_NOTEBOOK_LIST_ERROR =
  'code-notebook/HomePage/GET_LATEST_NOTEBOOK_LIST_ERROR';
export const ADD_NOTEBOOK = 'code-notebook/HomePage/ADD_NOTEBOOK';
export const PASSED = 'PASSED';
export const FAILED = 'FAILED';
export const ABORTED = 'ABORTED';
export const INCOMPLETE = 'INCOMPLETE';
export const COMPLETED = 'COMPLETED';
