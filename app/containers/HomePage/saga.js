/**
 * Gets the repositories of the user from Github
 */

import { put, takeLatest } from 'redux-saga/effects';
import { db, mergeResults } from 'utils/firebase';
import { GET_LATEST_NOTEBOOK_LIST } from './constants';
import { notebookListLoaded, notebookListLoadingError } from './actions';

export function* getLatestNotebookList(action) {
  try {
    const resultPublic = yield db
      .collection('books')
      .where('isPublic', '==', true)
      .orderBy('created_time')
      .get();
    if (action.userUid) {
      const resultUser = yield db
        .collection('books')
        .where('createdByUid', '==', action.userUid)
        .orderBy('created_time')
        .get();
      const result = mergeResults(resultPublic, resultUser, 'id');
      yield put(notebookListLoaded(result));
      return;
    }
    yield put(notebookListLoaded(resultPublic));
  } catch (err) {
    yield put(notebookListLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* latestNotebookListSagaManager() {
  // Fork watcher so we can continue execution
  yield takeLatest(GET_LATEST_NOTEBOOK_LIST, getLatestNotebookList);
}
