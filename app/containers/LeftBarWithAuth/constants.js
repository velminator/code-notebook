/*
 *
 * LeftBarWithAuth constants
 *
 */

export const DEFAULT_ACTION = 'app/LeftBarWithAuth/DEFAULT_ACTION';

export const SIGNIN_ACTION = 'app/LeftBarWithAuth/SIGNIN';

export const SIGNOUT_ACTION = 'app/LeftBarWithAuth/SIGNOUT';

export const SIGNIN_ACTION_ERROR = 'app/LeftBarWithAuth/SIGNIN_ERROR';

export const SIGNIN_ACTION_SUCCESS = 'app/LeftBarWithAuth/SIGNIN_SUCCESS';

export const SIGNOUT_ACTION_ERROR = 'app/LeftBarWithAuth/SIGNOUT_ERROR';

export const SIGNOUT_ACTION_SUCCESS = 'app/LeftBarWithAuth/SIGNOUT_SUCCESS';
