import { fromJS } from 'immutable';
import leftBarWithAuthReducer from '../reducer';

describe('leftBarWithAuthReducer', () => {
  it('returns the initial state', () => {
    expect(leftBarWithAuthReducer(undefined, {})).toEqual(fromJS({}));
  });
});
