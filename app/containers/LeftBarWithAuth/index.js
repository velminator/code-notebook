/**
 *
 * LeftBarWithAuth
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import Note from '@material-ui/icons/Note';
import Person from '@material-ui/icons/Person';
import PowerOffTwoTone from '@material-ui/icons/PowerOffTwoTone';
import Search from '@material-ui/icons/Search';
import Drawer from '@material-ui/core/Drawer';
import { auth } from 'utils/firebase';
import Snackbar from '@material-ui/core/Snackbar';
import {
  selectLoading,
  selectError,
  selectGetUser,
  selectGetIsLoggedIn,
} from './selectors';
import { signInAction, signOutAction, signInActionSuccess } from './actions';
import reducer from './reducer';
import { SignInSaga, SignOutSaga } from './saga';

const drawerWidth = '80px';
const styles = theme => ({
  root: {
    height: '100%',
  },
  bottomOption: {
    position: 'absolute',
    bottom: 0,
  },
  drawer: {
    left: drawerWidth,
    width: '440px',
    height: '100%',
    zIndex: 9,
    backgroundColor: '#F7F7F7',
  },
  modal: {
    position: 'absolute',
    left: drawerWidth,
  },
  icon: {
    marginBottom: theme.spacing.unit,
  },
  option: {
    boxShadow: 'none',
    backgroundColor: 'none',
    margin: '16px auto',
    width: '100%',
    display: 'inline-block',
    textAlign: 'center',
    cursor: 'pointer',
    border: 'none',
    textTransform: 'None',
  },
  userSettings: {
    width: '100%',
    height: '100%',
  },
  closeSnackbarIcon: {
    padding: theme.spacing.unit / 2,
  },
});

/* eslint-disable react/prefer-stateless-function */
export class LeftBarWithAuth extends React.Component {
  state = {
    userMenu: false,
    showSnackbar: false,
  };

  handleSnackBarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ showSnackbar: false });
  };

  toggleDrawer = () => {
    if (this.props.isLoggedIn) {
      this.setState(prevState => ({
        userMenu: !prevState.userMenu,
        showSnackbar: false,
      }));
    } else {
      this.props.signInAction();
    }
  };

  logout = () => {
    this.props.signOutAction();
    this.setState(prevState => ({
      userMenu: !prevState.userMenu,
      showSnackbar: false,
    }));
  };

  componentDidMount() {
    const self = this;
    auth.onAuthStateChanged(user => {
      if (user) {
        self.props.signInActionSuccess(user);
      } else {
        self.setState({
          showSnackbar: true,
        });
      }
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Drawer
          open={this.state.userMenu}
          onClose={this.toggleDrawer}
          classes={{ paper: classes.drawer, modal: classes.modal }}
        >
          <div className={classes.userSettings}>
            <Button
              variant="text"
              className={classes.option}
              onClick={this.logout}
            >
              <PowerOffTwoTone className={classes.icon} />
              Logout
            </Button>
          </div>
        </Drawer>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.showSnackbar}
          autoHideDuration={6000}
          onClose={this.handleSnackBarClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={
            <span id="message-id">Can create/update only when logged in!</span>
          }
          action={[
            <Button
              key="undo"
              color="secondary"
              size="small"
              onClick={this.toggleDrawer}
            >
              Login
            </Button>,
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.closeSnackbarIcon}
              onClick={this.handleSnackBarClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
        <div className={classes.root}>
          <Button
            component={Link}
            to="/"
            variant="text"
            className={classes.option}
          >
            <LibraryBooks className={classes.icon} />
            Books
          </Button>
          <Button
            component={Link}
            to="/notes/All Notes/all"
            variant="text"
            className={classes.option}
          >
            <Note className={classes.icon} />
            Notes
          </Button>
          <Button
            component={Link}
            to="/search"
            variant="text"
            className={classes.option}
          >
            <Search className={classes.icon} />
            Search
          </Button>
          <div className={classes.bottomOption}>
            <Button
              variant="text"
              className={classes.option}
              onClick={this.toggleDrawer}
            >
              <Person className={classes.icon} />
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

LeftBarWithAuth.propTypes = {
  // loading: PropTypes.bool,
  // error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  // user: PropTypes.object,
  isLoggedIn: PropTypes.bool,
  classes: PropTypes.object,
  signInAction: PropTypes.func,
  signOutAction: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  loading: selectLoading(),
  error: selectError(),
  user: selectGetUser(),
  isLoggedIn: selectGetIsLoggedIn(),
});

function mapDispatchToProps(dispatch) {
  return {
    signOutAction: () => dispatch(signOutAction()),
    signInAction: () => dispatch(signInAction()),
    signInActionSuccess: user => dispatch(signInActionSuccess(user)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'auth', reducer });
const withLogInSaga = injectSaga({ key: 'authLogin', saga: SignInSaga });
const withLogOutSaga = injectSaga({ key: 'authLogout', saga: SignOutSaga });

export default compose(
  withReducer,
  withLogInSaga,
  withLogOutSaga,
  withConnect,
)(withStyles(styles)(LeftBarWithAuth));
