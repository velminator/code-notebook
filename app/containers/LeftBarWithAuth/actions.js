/*
 *
 * LeftBarWithAuth actions
 *
 */

import {
  DEFAULT_ACTION,
  SIGNIN_ACTION,
  SIGNOUT_ACTION,
  SIGNIN_ACTION_ERROR,
  SIGNIN_ACTION_SUCCESS,
  SIGNOUT_ACTION_ERROR,
  SIGNOUT_ACTION_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function signInAction() {
  return {
    type: SIGNIN_ACTION,
  };
}

export function signInActionError(error) {
  return {
    type: SIGNIN_ACTION_ERROR,
    error,
  };
}

export function signInActionSuccess(user) {
  return {
    type: SIGNIN_ACTION_SUCCESS,
    user,
  };
}

export function signOutAction() {
  return {
    type: SIGNOUT_ACTION,
  };
}

export function signOutActionSuccess() {
  return {
    type: SIGNOUT_ACTION_SUCCESS,
  };
}

export function signOutActionError(error) {
  return {
    type: SIGNOUT_ACTION_ERROR,
    error,
  };
}
