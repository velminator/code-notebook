import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the leftBarWithAuth state domain
 */

const selectLeftBarWithAuthDomain = state =>
  state.get('leftBarWithAuth', initialState);

export const selectGetUser = () =>
  createSelector(selectLeftBarWithAuthDomain, substate => substate.get('user'));

export const selectGetIsLoggedIn = () =>
  createSelector(selectLeftBarWithAuthDomain, substate =>
    substate.get('isLoggedIn'),
  );

export const selectLoading = () =>
  createSelector(selectLeftBarWithAuthDomain, substate =>
    substate.get('loading'),
  );

export const selectError = () =>
  createSelector(selectLeftBarWithAuthDomain, substate =>
    substate.get('error'),
  );
