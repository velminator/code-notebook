/**
 *
 * UserAuth
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { auth } from 'utils/firebase';
import {
  selectLoading,
  selectError,
  selectGetUser,
  selectGetIsLoggedIn,
} from './selectors';
import { signInAction, signOutAction, signInActionSuccess } from './actions';
import reducer from './reducer';
import { SignInSaga, SignOutSaga } from './saga';

export default AuthRequiredComponent => {
  /* eslint-disable react/prefer-stateless-function */
  class UserAuth extends React.Component {
    componentWillMount() {
      const self = this;
      auth.onAuthStateChanged(user => {
        if (user) {
          self.props.authSignInActionSuccess(user);
        }
      });
      const user = auth.currentUser;
      if (user) {
        self.props.authSignInActionSuccess(user);
        // this.props.authUser = user;
      }
    }

    render() {
      return <AuthRequiredComponent {...this.props} />;
    }
  }

  UserAuth.propTypes = {
    dispatch: PropTypes.func,
  };

  const mapStateToProps = createStructuredSelector({
    authLoading: selectLoading(),
    authError: selectError(),
    authUser: selectGetUser(),
    authIsLoggedIn: selectGetIsLoggedIn(),
  });

  function mapDispatchToProps(dispatch) {
    return {
      authSignOutAction: () => dispatch(signOutAction()),
      authSignInAction: () => dispatch(signInAction()),
      authSignInActionSuccess: user => dispatch(signInActionSuccess(user)),
    };
  }

  const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
  );

  const withReducer = injectReducer({ key: 'userAuth', reducer });
  const withLogInSaga = injectSaga({ key: 'authLogin', saga: SignInSaga });
  const withLogOutSaga = injectSaga({ key: 'authLogout', saga: SignOutSaga });

  return compose(
    withReducer,
    withLogInSaga,
    withLogOutSaga,
    withConnect,
  )(UserAuth);
};
