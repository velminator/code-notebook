/*
 *
 * UserAuth reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SIGNIN_ACTION,
  SIGNOUT_ACTION,
  SIGNIN_ACTION_ERROR,
  SIGNIN_ACTION_SUCCESS,
  SIGNOUT_ACTION_ERROR,
  SIGNOUT_ACTION_SUCCESS,
} from './constants';

export const initialState = fromJS({
  user: {},
  isLoggedIn: false,
  loading: false,
  error: false,
});

function userAuthReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SIGNOUT_ACTION:
      return state
        .set('isLoggedIn', false)
        .set('user', {})
        .set('error', false)
        .set('loading', false);
    case SIGNIN_ACTION:
      return state
        .set('isLoggedIn', false)
        .set('user', {})
        .set('error', false)
        .set('loading', true);
    case SIGNIN_ACTION_SUCCESS:
      return state
        .set('isLoggedIn', true)
        .set('user', action.user)
        .set('error', false)
        .set('loading', false);
    case SIGNIN_ACTION_ERROR:
      return state
        .set('isLoggedIn', false)
        .set('user', {})
        .set('error', action.error)
        .set('loading', false);
    case SIGNOUT_ACTION_ERROR:
      return state
        .set('isLoggedIn', true)
        .set('error', action.error)
        .set('user', {})
        .set('loading', false);
    case SIGNOUT_ACTION_SUCCESS:
      return state
        .set('isLoggedIn', false)
        .set('user', {})
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default userAuthReducer;
