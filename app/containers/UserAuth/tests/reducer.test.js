import { fromJS } from 'immutable';
import userAuthReducer from '../reducer';

describe('userAuthReducer', () => {
  it('returns the initial state', () => {
    expect(userAuthReducer(undefined, {})).toEqual(fromJS({}));
  });
});
