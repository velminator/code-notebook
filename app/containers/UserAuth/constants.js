/*
 *
 * UserAuth constants
 *
 */

export const DEFAULT_ACTION = 'app/UserAuth/DEFAULT_ACTION';

export const SIGNIN_ACTION = 'app/UserAuth/SIGNIN';

export const SIGNOUT_ACTION = 'app/UserAuth/SIGNOUT';

export const SIGNIN_ACTION_ERROR = 'app/UserAuth/SIGNIN_ERROR';

export const SIGNIN_ACTION_SUCCESS = 'app/UserAuth/SIGNIN_SUCCESS';

export const SIGNOUT_ACTION_ERROR = 'app/UserAuth/SIGNOUT_ERROR';

export const SIGNOUT_ACTION_SUCCESS = 'app/UserAuth/SIGNOUT_SUCCESS';
