import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the userAuth state domain
 */

const selectUserAuthDomain = state => state.get('userAuth', initialState);

export const selectGetUser = () =>
  createSelector(selectUserAuthDomain, substate => substate.get('user'));

export const selectGetIsLoggedIn = () =>
  createSelector(selectUserAuthDomain, substate => substate.get('isLoggedIn'));

export const selectLoading = () =>
  createSelector(selectUserAuthDomain, substate => substate.get('loading'));

export const selectError = () =>
  createSelector(selectUserAuthDomain, substate => substate.get('error'));
