import { put, takeLatest } from 'redux-saga/effects';
import { auth, provider } from 'utils/firebase';
import { SIGNIN_ACTION, SIGNOUT_ACTION } from './constants';
import {
  signInActionSuccess,
  signInActionError,
  signOutActionSuccess,
  signOutActionError,
} from './actions';

// Individual exports for testing
export function* getAuthUser() {
  try {
    const { user } = yield auth.signInWithPopup(provider);
    yield put(signInActionSuccess(user));
  } catch (error) {
    yield put(signInActionError(error));
  }
}

export function* SignInSaga() {
  yield takeLatest(SIGNIN_ACTION, getAuthUser);
}

export function* logOutAuthUser() {
  try {
    yield auth.signOut();
    yield put(signOutActionSuccess());
  } catch (error) {
    yield put(signOutActionError(error));
  }
}

export function* SignOutSaga() {
  yield takeLatest(SIGNOUT_ACTION, logOutAuthUser);
}
