/*
 *
 * PageHome actions
 *
 */

import {
  DEFAULT_ACTION,
  GET_LATEST_PAGE_LIST,
  GET_LATEST_PAGE_LIST_SUCCESS,
  GET_LATEST_PAGE_LIST_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getLatestPageList(bookId, authUserUid) {
  return {
    type: GET_LATEST_PAGE_LIST,
    bookId,
    authUserUid,
  };
}

/**
 * Dispatched when the Notebooks are loaded by the request saga
 *
 * @param  {array} pages array
 *
 * @return {object}      An action object with a type of GET_LATEST_PAGE_LIST_SUCCESS passing the repos
 */
export function pageListLoaded(snapshot) {
  const data = [];
  snapshot.forEach(record => {
    data.push(record);
  });
  return {
    type: GET_LATEST_PAGE_LIST_SUCCESS,
    data,
  };
}

/**
 * Dispatched when loading the pageList fails
 *
 * @param  {object} error The error
 *
 * @return {object} An action object with a type of GET_LATEST_PAGE_LIST_ERROR passing the error
 */
export function pageListLoadingError(error) {
  return {
    type: GET_LATEST_PAGE_LIST_ERROR,
    error,
  };
}
