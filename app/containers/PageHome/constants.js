/*
 *
 * PageHome constants
 *
 */

export const DEFAULT_ACTION = 'app/PageHome/DEFAULT_ACTION';

export const GET_LATEST_PAGE_LIST =
  'code-notebook/PageHome/GET_LATEST_PAGE_LIST';

export const GET_LATEST_PAGE_LIST_SUCCESS =
  'code-notebook/PageHomeGET_LATEST_PAGE_LIST_SUCCESS';

export const GET_LATEST_PAGE_LIST_ERROR =
  'code-notebook/PageHomeGET_LATEST_PAGE_LIST_ERROR';
export const ADD_PAGE = 'code-notebook/PageHome/ADD_PAGE';
export const PASSED = 'PASSED';
export const FAILED = 'FAILED';
export const ABORTED = 'ABORTED';
export const INCOMPLETE = 'INCOMPLETE';
export const COMPLETED = 'COMPLETED';
