import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pageHome state domain
 */

const selectPageHomeDomain = state => state.get('pageHome', initialState);

/**
 * Other specific selectors
 */
const selectGetPageList = () =>
  createSelector(selectPageHomeDomain, PageHomeState =>
    PageHomeState.get('pageList'),
  );

const selectPageLoading = () =>
  createSelector(selectPageHomeDomain, PageHomeState =>
    PageHomeState.get('loading'),
  );

const selectPageError = () =>
  createSelector(selectPageHomeDomain, PageHomeState =>
    PageHomeState.get('error'),
  );

/**
 * Default selector used by PageHome
 */

const makeSelectPageHome = () =>
  createSelector(selectPageHomeDomain, substate => substate.toJS());

export default makeSelectPageHome;
export {
  selectPageHomeDomain,
  selectGetPageList,
  selectPageLoading,
  selectPageError,
};
