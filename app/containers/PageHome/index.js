/**
 *
 * PageHome
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import PageList from 'components/PageList';
import AddPage from 'components/AddPage';
import withUserAuth from 'containers/UserAuth';
import reducer from './reducer';
import saga from './saga';
import {
  selectGetPageList,
  selectPageLoading,
  selectPageError,
} from './selectors';
import { getLatestPageList } from './actions';

const styles = {
  notesContainer: {
    flexGrow: 1,
    padding: '10px',
  },
  title: {
    width: '80vw',
    padding: '10px',
  },
};

/* eslint-disable react/prefer-stateless-function */
export class PageHome extends React.PureComponent {
  componentDidMount() {
    const { authUser } = this.props;
    const { uid } = authUser;
    if (this.props.match.params.bookId) {
      this.props.getLatestPageList(this.props.match.params.bookId, uid);
    }
  }

  renderResults() {
    if (this.props.loading) {
      return (
        <Grid item xs={12}>
          <div style={{ textAlign: 'center' }}>
            <CircularProgress size={80} />
          </div>
        </Grid>
      );
    }
    if (this.props.error) {
      return <h1> Error! </h1>;
    }
    return (
      <PageList
        pageList={this.props.pageList}
        refresh={this.props.getLatestPageList}
        bookId={this.props.match.params.bookId}
      />
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.notesContainer}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Typography
              classes={{ root: classes.title }}
              variant="h6"
              gutterBottom
              align="center"
            >
              {this.props.match.params.bookName}
            </Typography>
          </Grid>
          {this.renderResults()}
        </Grid>
        <AddPage
          bookId={this.props.match.params.bookId}
          refresh={this.props.getLatestPageList}
        />
      </div>
    );
  }
}

PageHome.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  getLatestPageList: PropTypes.func,
  pageList: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  match: PropTypes.object,
  classes: PropTypes.object,
  authUser: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loading: selectPageLoading(),
  error: selectPageError(),
  pageList: selectGetPageList(),
});

function mapDispatchToProps(dispatch) {
  return {
    getLatestPageList: (bookId, authUserUid) =>
      dispatch(getLatestPageList(bookId, authUserUid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'pageHome', reducer });
const withSaga = injectSaga({ key: 'pageHome', saga });

export default withUserAuth(
  compose(
    withReducer,
    withSaga,
    withConnect,
  )(withStyles(styles)(PageHome)),
);
