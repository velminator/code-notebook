/**
 *
 * Asynchronously loads the component for PageHome
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
