/*
 *
 * PageHome reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  GET_LATEST_PAGE_LIST,
  GET_LATEST_PAGE_LIST_SUCCESS,
  GET_LATEST_PAGE_LIST_ERROR,
} from './constants';

export const initialState = fromJS({
  loading: false,
  error: false,
  pageList: [],
});

function pageHomeReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_LATEST_PAGE_LIST:
      return state
        .set('loading', true)
        .set('error', false)
        .set('pageList', []);
    case GET_LATEST_PAGE_LIST_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('pageList', action.data);
    case GET_LATEST_PAGE_LIST_ERROR:
      return state.set('error', action.error).set('loading', false);
    default:
      return state;
  }
}

export default pageHomeReducer;
