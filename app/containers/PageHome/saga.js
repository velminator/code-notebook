import { takeLatest, put } from 'redux-saga/effects';
import { db, mergeResults } from 'utils/firebase';
import { GET_LATEST_PAGE_LIST } from './constants';
import { pageListLoaded, pageListLoadingError } from './actions';

window.dbFirestore = db;

export function* getLatestPageList(action) {
  try {
    const resultPublic = yield db
      .collection('notes')
      .where('book', '==', db.doc(`books/${action.bookId}`))
      .where('isPublic', '==', true)
      .orderBy('created_time')
      .get();
    if (action.authUserUid) {
      const resultUser = yield db
        .collection('notes')
        .where('book', '==', db.doc(`books/${action.bookId}`))
        .where('isPublic', '==', false)
        .where('createdByUid', '==', action.authUserUid)
        .orderBy('created_time')
        .get();
      const result = mergeResults(resultPublic, resultUser, 'id');
      yield put(pageListLoaded(result));
      return;
    }
    yield put(pageListLoaded(resultPublic));
  } catch (err) {
    yield put(pageListLoadingError(err));
  }
}

export default function* pageHomeSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(GET_LATEST_PAGE_LIST, getLatestPageList);
}
