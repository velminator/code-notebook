import { fromJS } from 'immutable';
import pageHomeReducer from '../reducer';

describe('pageHomeReducer', () => {
  it('returns the initial state', () => {
    expect(pageHomeReducer(undefined, {})).toEqual(fromJS({}));
  });
});
