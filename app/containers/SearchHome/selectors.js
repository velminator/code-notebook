import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the searchHome state domain
 */

const selectSearchHomeDomain = state => state.get('searchHome', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by SearchHome
 */

const makeSelectSearchHome = () =>
  createSelector(selectSearchHomeDomain, substate => substate.toJS());

export default makeSelectSearchHome;
export { selectSearchHomeDomain };
