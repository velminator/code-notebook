import { fromJS } from 'immutable';
import searchHomeReducer from '../reducer';

describe('searchHomeReducer', () => {
  it('returns the initial state', () => {
    expect(searchHomeReducer(undefined, {})).toEqual(fromJS({}));
  });
});
