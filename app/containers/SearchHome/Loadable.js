/**
 *
 * Asynchronously loads the component for SearchHome
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
