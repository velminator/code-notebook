/**
 *
 * SearchHome
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
// import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectSearchHome from './selectors';
import reducer from './reducer';
import saga from './saga';

const styles = theme => ({
  notesContainer: {
    flexGrow: 1,
    padding: '10px',
  },
  button: {
    margin: theme.spacing.unit,
  },
  title: {
    width: '80vw',
    padding: '10px',
  },
});

/* eslint-disable react/prefer-stateless-function */
export class SearchHome extends React.Component {
  state = {
    searchType: 'Title',
    searchText: '',
  };

  handleSearchTextChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSearchTypeClick = name => () => {
    this.setState({
      searchType: name,
    });
  };

  render() {
    const { classes } = this.props;
    const { searchText, searchType } = this.state;
    return (
      <div className={classes.notesContainer}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <TextField
              id="standard-full-width"
              label="Search"
              value={searchText}
              style={{ margin: 8 }}
              placeholder="Enter Search Text"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              onChange={this.handleSearchTextChange('searchText')}
            />
            <Button
              onClick={this.handleSearchTypeClick('Title')}
              color={searchType === 'Title' ? 'primary' : 'default'}
              className={classes.button}
            >
              Title
            </Button>
            <Button
              onClick={this.handleSearchTypeClick('Tags')}
              color={searchType === 'Tags' ? 'primary' : 'default'}
              className={classes.button}
            >
              Tags
            </Button>
            <Button
              onClick={this.handleSearchTypeClick('Text')}
              color={searchType === 'Text' ? 'primary' : 'default'}
              className={classes.button}
            >
              Text
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

SearchHome.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  searchHome: makeSelectSearchHome(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'searchHome', reducer });
const withSaga = injectSaga({ key: 'searchHome', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(withStyles(styles)(SearchHome));
