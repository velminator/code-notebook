/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { Helmet } from 'react-helmet';
import { withStyles } from '@material-ui/core/styles';
import PageHome from 'containers/PageHome/Loadable';
// import SearchHome from 'containers/SearchHome/Loadable';
import LeftBar from 'components/LeftBar';
import GlobalStyle from '../../global-styles';

const styles = {
  root: {
    flexGrow: 1,
    width: '100vw',
    display: 'block',
    overflow: 'hidden',
    top: 0,
    left: 0,
    margin: 0,
    padding: 0,
  },
  content: {
    position: 'absolute' /* Same as the width of the sidebar */,
    left: '80px',
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#F7F7F7',
    overflowX: 'hidden',
  },
  leftBar: {
    position: 'fixed',
    width: '80px',
    left: 0,
    top: 0,
    backgroundColor: '#F0F0F0',
    zIndex: 10,
    overflow: 'hidden',
    height: '100vh',
  },
};

function App(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Helmet titleTemplate="Code Notebooks" defaultTitle="Code Notebooks" />
      <div className={classes.leftBar}>
        <LeftBar />
      </div>
      <div className={classes.content}>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/notes/:bookName/:bookId" component={PageHome} />
          <Route path="/search/" component={NotFoundPage} />
        </Switch>
      </div>
      <GlobalStyle />
    </div>
  );
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
