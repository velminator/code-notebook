

# Code-Notebooks
A react based notes app which can be used to create public/private content using code repls and markdown text

## Lifecycle of React-Redux-Saga
![enter image description here](https://docs.google.com/drawings/d/e/2PACX-1vQqNWV4VRpD4PTJsb8zsBk7IrPhUsBiNQOdUMyvhs-6lmSz7fMKaAzMA7hwcx9rjmJxS19E3ZFqizbi/pub?w=960&h=720)

## Documentation for the boilerplate

- [**The Hitchhikers Guide to `react-boilerplate`**](docs/general/introduction.md): An introduction for newcomers to this boilerplate.
- [Overview](docs/general): A short overview of the included tools
- [**Commands**](docs/general/commands.md): Getting the most out of this boilerplate
- [Testing](docs/testing): How to work with the built-in test harness
- [Styling](docs/css): How to work with the CSS tooling
- [Your app](docs/js): Supercharging your app with Routing, Redux, simple
  asynchronicity helpers, etc.
- [**Troubleshooting**](docs/general/gotchas.md): Solutions to common problems faced by developers.

